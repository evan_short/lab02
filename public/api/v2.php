<?php
$user1 = array("username" => "uno", "password" => "pass");
$user2 = array("username" => "dos", "password" => "pass");
$user3 = array("username" => "tres", "password" => "pass");

$users = array($user1, $user2, $user3);
$pages = array("page1.html", "page2.html", "page3.html");

$page = array_search($_POST, $users);
if($page===false){
  $page = "userNotFound.html";
}else{
  $page = $pages[$page];
}

header("location: /$page");
