<?php
class User {
  static private $users = [];

  public function __construct($name, $pass, $page){  
    $this->name = $name;
    $this->pass = $pass;
    $this->page = $page;
  }

  public static function create($name, $pass, $page){
    $user = new User($name, $pass, $page);
    array_push(User::$users, $user);
    return $user;
  }

  public static function find($name, $pass){
    foreach(User::$users as $user){
      if($name == $user->name && $pass == $user->pass){
        return $user;
      }
    }
    throw(new Exception('user not found'));
  }
}

User::create('uno', 'pass', 'page1.html');
User::create('dos', 'pass', 'page2.html');
User::create('tres', 'pass', 'page3.html');
User::create('another', 'pass', 'page3.html');

try{
  $user = User::find($_POST['username'], $_POST['password']);
  $location = $user->page;
}catch(Exception $e){
  $location = 'userNotFound.html';
}

header("location: /$location");
