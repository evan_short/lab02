<?php
class DB {
  static private $db;
  static private $mysqli;

  public function connect(){
    $db = DB::$db;
    $mysqli = new \mysqli(
      $db->host,
      $db->user,
      $db->password,
      $db->database,
      $db->port
    );
    $mysqli->set_charset("utf8");

    DB::$mysqli = $mysqli;
    return $mysqli;
  }

  static public function setup($host, $user, $pass, $database, $port=3306){
    $db = DB::$db = new stdClass();

    $db->host     = $host;
    $db->user     = $user;
    $db->password = $pass;
    $db->database = $database;
    $db->por      = $port;
  }
}
?>
