<?php

include("user.php");
include("db_connect.php");

DB::setup('127.0.0.1', 'root', 'root', 'scotchbox');

switch($_SERVER['REQUEST_METHOD']){
  case 'POST':
    logUserIn($_POST);
    break;
}

function logUserIn($name, $pass){
  try{
    $user = User::find($_POST['username'], $_POST['password']);
    $location = $user->page;
  }catch(Exception $e){
    error_log($e);
    $location = 'userNotFound.html';
  }
  header("location: /$location");
}
