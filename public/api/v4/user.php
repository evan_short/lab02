<?php
class User {
  static private $users = [];

  public function __construct($name, $pass, $page){  
    $this->name = $name;
    $this->pass = $pass;
    $this->page = $page;
  }

  public static function find($name, $pass){
    $mysql_link = DB::connect();

    $result = $mysql_link->query("
      SELECT
        username, page 
      FROM
        users
      WHERE username = '$name'
      AND   password = '$pass'
      LIMIT 1
    ");

    if($mysql_link->error) throw new Exception($mysql_link->error);

    if($result->num_rows == 0) throw new Exception('user not found');

    $user = $result->fetch_object();

    return $user;
  }
}
?>
