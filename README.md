CGT356 LAB02
------------

To run this project, install it on a webserver by 
cloning its contents.

`git clone git@bitbucket.org:evan_short/lab02.git`

or by downloading the source and unzipping it on your server

If you do not have access to an acceptable webserver, I 
personally suggest using Vagrant as a local alternative. I
have included the necessary items so as to make running a 
vagrant server as simple as installing the required free
and highly reliable software

https://www.virtualbox.org/wiki/Downloads

https://www.vagrantup.com/downloads.html

and then running 

`vagrant up`

If you choose to use vagrant, your application will be visible to your browser at http://192.168.33.10/

Tailing Logs
------------

tailing a file is a command-line way of saying "display the end 
of a file as it happens. On mac, linux, and other UNIX based computers
there is a command line application called `tail` which can be used
to watch error logs in real time. Very useful for debugging. I don't
know of an exact equivalent on windows, but at a worst case scenario
you can always just open the error log to read it.

http://www.computerhope.com/unix/utail.htm

##Apache with .htaccess recognized

the public directory has a .htaccess file which requests all 
php scripts report errors to a file called `error_log` that is 
inside the same directory as the file that initiated the script
that caused the error. I am looking into simplifying this,
but for the purposes of this lab there are not so many places
the error log could end up that it is a showstopper.

##On Vagrant

the error log:

`vagrant ssh -c "tail -f /var/log/apache2/error.log"`

the access log:

`vagrant ssh -c "tail -f /var/log/apache2/access.log"`
